package nl.finalist.javaevolution.java12;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class FileMismatchTest {

    @Test
    public void testFileMismatchEqual() throws IOException {
        Path tempPath1 = Files.createTempFile("test", ".txt");
        tempPath1.toFile().deleteOnExit();
        Files.writeString(tempPath1, "some string");

        Path tempPath2 = Files.createTempFile("test", ".txt");
        tempPath2.toFile().deleteOnExit();
        Files.writeString(tempPath2, "some string");

        assertThat(Files.mismatch(tempPath1, tempPath2), is(-1L));
    }

    @Test
    public void testFileMismatchNotEqual() throws IOException {
        Path tempPath1 = Files.createTempFile("test", ".txt");
        tempPath1.toFile().deleteOnExit();
        Files.writeString(tempPath1, "some string");

        Path tempPath2 = Files.createTempFile("test", ".txt");
        tempPath2.toFile().deleteOnExit();
        Files.writeString(tempPath2, "some other string");

        assertThat(Files.mismatch(tempPath1, tempPath2), is(5L));
    }
}