package nl.finalist.javaevolution.java12;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class NewStringMethodTest {

    @Test
    public void testStringMethods() {
        // Inspringen van tekst
        assertThat("""
                   Make a multiline string (from Java 13)
                   And add some indentation
                   """.indent(4), is("""
                                         Make a multiline string (from Java 13)
                                         And add some indentation
                                     """));

        // Eenvoudig transformeren
        assertThat("dit is"
            .transform(s -> s.concat(" een test")
            .transform(String::toUpperCase)), is("DIT IS EEN TEST"));

        // String gebruikt nu de ConstantDesc interface, voegt verder weinig toe
        String value = "test";
        assertThat(value.describeConstable(), is(Optional.of("test")));
        assertThat(value.resolveConstantDesc(null), is("test"));
    }
}