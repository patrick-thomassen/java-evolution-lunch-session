package nl.finalist.javaevolution.java12;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CompactNumberFormatTest {

    @Test
    public void testCompactNumberFormatShort() {
        List<Integer> numbers = List.of(1000, 1000000, 1000000000);

        NumberFormat nf = NumberFormat.getCompactNumberInstance(Locale.ENGLISH, NumberFormat.Style.SHORT);

        List<String> output =  numbers.stream().map((num) -> nf.format(num)).toList();

        assertThat(output, is(List.of("1K", "1M", "1B")));
    }

    @Test
    public void testCompactNumberFormatLong() {
        List<Integer> numbers = List.of(1000, 1000000, 1000000000);

        NumberFormat nf = NumberFormat.getCompactNumberInstance(Locale.ENGLISH, NumberFormat.Style.LONG);

        List<String> output =  numbers.stream().map((num) -> nf.format(num)).toList();

        assertThat(output, is(List.of("1 thousand", "1 million", "1 billion")));
    }
}