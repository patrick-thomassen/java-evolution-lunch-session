package nl.finalist.javaevolution.java12;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class TeeingCollectorTest {
    @Test
    public void testTeeingCollector() throws IOException {
        Employee jan = new Employee(1, "Jan", 1300);
        Employee henk = new Employee(2, "Henk", 1700);
        Employee piet = new Employee(3, "Piet", 1200);
        Employee stefan = new Employee(4, "Stefan", 1400);
        List<Employee> employeeList = List.of(jan, henk, piet, stefan);

        Map<String, Employee> result = employeeList.stream().collect( 
            Collectors.teeing(
                Collectors.maxBy(Comparator.comparing(Employee::getSalary)),
                Collectors.minBy(Comparator.comparing(Employee::getSalary)),
                (e1, e2) -> Map.of("MAX", e1.get(), "MIN", e2.get())));

        assertThat(result.get("MIN"), is(piet));
        assertThat(result.get("MAX"), is(henk));
    }
}