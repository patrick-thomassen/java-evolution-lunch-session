package nl.finalist.javaevolution.java12;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.finalist.javaevolution.java12.SwitchExpressions.Smaak;

@ExtendWith(MockitoExtension.class)
public class SwitchExpressionsTest {

    @Test
    public void testGetPrijs() {
        assertThat(SwitchExpressions.getPrijs(Smaak.CHOCOLADE), is(3));
        assertThat(SwitchExpressions.getPrijs(Smaak.KOKOS), is(3));
        assertThat(SwitchExpressions.getPrijs(Smaak.VANILLE), is(4));
        assertThat(SwitchExpressions.getPrijs(Smaak.AARDBEI), is(5));
        assertThat(SwitchExpressions.getPrijs(Smaak.FRAMBOOS), is(6));
    }

    @Test
    public void testGetPrijsWithYield() {
        assertThat(SwitchExpressions.getPrijsWithYield(Smaak.CHOCOLADE), is(3));
        assertThat(SwitchExpressions.getPrijsWithYield(Smaak.KOKOS), is(3));
        assertThat(SwitchExpressions.getPrijsWithYield(Smaak.VANILLE), is(4));
        assertThat(SwitchExpressions.getPrijsWithYield(Smaak.AARDBEI), is(5));
        assertThat(SwitchExpressions.getPrijsWithYield(Smaak.FRAMBOOS), is(6));
    }
}