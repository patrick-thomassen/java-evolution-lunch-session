package nl.finalist.javaevolution.java16;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class DayPeriodSupportTest {
    @Test
    public void testGetPeriodOfDay() {
        assertThat(DayPeriodSupport.getPeriodOfDay(), is("3 ’s middags")); // in the afternoon
    }
}