package nl.finalist.javaevolution.java16;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class StreamToListTest {
    @Test
    public void testCar() {
        assertThat(StreamToList.getList(), hasItems("Dog", "Horse"));
    }
}