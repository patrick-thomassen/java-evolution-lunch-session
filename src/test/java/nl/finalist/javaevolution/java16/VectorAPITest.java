package nl.finalist.javaevolution.java16;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import jdk.incubator.vector.FloatVector;

@ExtendWith(MockitoExtension.class)
public class VectorAPITest {
    @Test
    public void testCar() {
        FloatVector vector = VectorAPI.getResult();
        assertThat(vector.toArray(), is(List.of(-36.0f, -100.0f, -169.0f, -256.0f).toArray()));
    }
}