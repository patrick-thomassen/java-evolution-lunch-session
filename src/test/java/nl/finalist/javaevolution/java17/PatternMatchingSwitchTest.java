package nl.finalist.javaevolution.java17;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.finalist.javaevolution.java14.Bird;
import nl.finalist.javaevolution.java14.Cat;
import nl.finalist.javaevolution.java14.Dog;
import nl.finalist.javaevolution.java14.Fish;

@ExtendWith(MockitoExtension.class)
public class PatternMatchingSwitchTest {
    @Test
    public void testGetValue() {
        assertThat(PatternMatchingSwitch.getValue(1), is("int 1"));
        assertThat(PatternMatchingSwitch.getValue(2L), is("long 2"));
        assertThat(PatternMatchingSwitch.getValue(3.0d), is("double 3,000000"));
        assertThat(PatternMatchingSwitch.getValue(4.0f), is("4.0")); // Default case
        assertThat(PatternMatchingSwitch.getValue("Java 17"), is("String Java 17"));
    }

    @Test
    public void testGiveCommandWithSwitch() {
        PatternMatchingSwitch.giveCommandWithSwitch(null);
        PatternMatchingSwitch.giveCommandWithSwitch(new Cat());
        PatternMatchingSwitch.giveCommandWithSwitch(new Dog());
        PatternMatchingSwitch.giveCommandWithSwitch(new Fish(false));
        PatternMatchingSwitch.giveCommandWithSwitch(new Fish(true));

        IllegalStateException exception = assertThrows(IllegalStateException.class, () -> {
            PatternMatchingSwitch.giveCommandWithSwitch(new Bird());
        });

        assertThat(exception.getMessage(), is("Unknown type of animal"));
    }
}