package nl.finalist.javaevolution.java11;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CollectionToArrayTest {
    @Test
    public void testGetNames() {
        assertThat(CollectionToArray.getNames(), is(List.of("Jan", "Henk", "Sander").toArray()));
    }
}