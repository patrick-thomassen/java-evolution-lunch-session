package nl.finalist.javaevolution.java11;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class HttpClientTest {
    @Test
    public void testGetBodyOfUrl() throws IOException, InterruptedException {
        // Vervang met je eigen ip adres
        assertThat(JavaHttpClient.getBodyOfUrl("https://api.ipify.org"), is("145.131.134.66"));
    }
}