package nl.finalist.javaevolution.java11;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class LocalVariableLambdaTest {
    @Test
    public void testUpperCaseAll() {
        assertThat(LocalVariableLambda.upperCaseAll(List.of("groen", "rood", "blauw")), is(List.of("GROEN", "ROOD", "BLAUW")));
    }
}