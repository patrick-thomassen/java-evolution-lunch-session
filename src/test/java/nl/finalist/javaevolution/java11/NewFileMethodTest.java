package nl.finalist.javaevolution.java11;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class NewFileMethodTest {
    @Test
    public void testCar() throws IOException {
        assertThat(NewFileMethod.writeAndReadStringToFile("Dit is een tijdelijk bestand"), is("Dit is een tijdelijk bestand"));
    }
}
