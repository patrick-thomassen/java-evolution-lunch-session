package nl.finalist.javaevolution.java11;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class NewStringMethodTest {

    @Test
    public void test() {
        // lines()
        Stream<String> lines = "Dit\nworden\nvier\nwaarden".lines();
        assertThat(lines.toList(), is(List.of("Dit", "worden", "vier", "waarden")));

        // isBlank
        assertThat("".isBlank(), is(true));
        assertThat(" ".isBlank(), is(true));
        assertThat("test".isBlank(), is(false));

        // stripLeading()
        assertThat("   Dit is een regel met ruimte aan het begin en aan het eind   ".stripLeading(), is("Dit is een regel met ruimte aan het begin en aan het eind   "));

        // stripTrailing()
        assertThat("   Dit is een regel met ruimte aan het begin en aan het eind   ".stripTrailing(), is("   Dit is een regel met ruimte aan het begin en aan het eind"));
    }
}