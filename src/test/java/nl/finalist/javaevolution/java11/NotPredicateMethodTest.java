package nl.finalist.javaevolution.java11;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class NotPredicateMethodTest {
    @Test
    public void test() {
        List<String> values = List.of("Waarde", "Andere waarde", "", "Nog een waarde", " ");
        assertThat(NotPredicateMethod.filterBlank(values), is(List.of("Waarde", "Andere waarde", "Nog een waarde")));
    }
}