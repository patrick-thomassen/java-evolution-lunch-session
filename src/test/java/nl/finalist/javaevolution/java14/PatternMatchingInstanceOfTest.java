package nl.finalist.javaevolution.java14;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class PatternMatchingInstanceOfTest {
    @Test
    public void testGiveCommand() {
        PatternMatchingInstanceOf.giveCommand(new Cat());
        PatternMatchingInstanceOf.giveCommand(new Dog());
    }
}