package nl.finalist.javaevolution.java14;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CarRecordTest {
    @Test
    public void testCar() {
        Car ferrari = new Car("Ferrari", "SF90 Stradale");
        assertThat(ferrari.brand(), is("Ferrari"));
        assertThat(ferrari.model(), is("SF90 Stradale"));
        assertThat(ferrari.toString(), is("Car[brand=Ferrari, model=SF90 Stradale]"));

        Car ferrari2 = new Car("Ferrari", "SF90 Stradale");
        assertThat(ferrari2, is(ferrari));

        Car ford = new Car("Ford", "Mustang GT");
        assertThat(ferrari2, is(not(ford)));
    }
}