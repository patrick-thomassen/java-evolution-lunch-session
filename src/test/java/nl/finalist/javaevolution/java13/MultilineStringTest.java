package nl.finalist.javaevolution.java13;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MultilineStringTest {
    @Test
    public void testGetHeader() {
        assertThat(MultilineString.getHeader(), is("Java\nFinally\nSupports\nMultiline\nStrings\n"));
    }
}