package nl.finalist.javaevolution.java15;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class NewStringMethodTest {
    @Test
    public void test() {
        // formatting() (String.format maar dan niet static)
        assertThat("Dit lijkt op %s".formatted("String.format"), is("Dit lijkt op String.format"));

        // stripIndent
        assertThat("    hello\n    world".stripIndent(), is("hello\nworld"));
        assertThat("    hello\n     world".stripIndent(), is("hello\n world"));

        // translateEscapes()
        assertThat("\\\\".translateEscapes(), is("\\"));
        assertThat("\\n".translateEscapes(), is("\n"));
        assertThat("\\n", is("\\n"));
    }
}