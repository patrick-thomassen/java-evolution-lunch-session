package nl.finalist.javaevolution.java15;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.finalist.javaevolution.java13.NullPointer;

@ExtendWith(MockitoExtension.class)
public class NullPointerExceptionTest {
    @Test
    public void testGetNull() {
        NullPointerException exception = assertThrows(NullPointerException.class, () -> {
            NullPointer.getNull().toString();
        });

        assertThat(exception.getMessage(), is("Cannot invoke \"String.toString()\" because the return value of \"nl.finalist.javaevolution.java13.NullPointer.getNull()\" is null"));
    }
}