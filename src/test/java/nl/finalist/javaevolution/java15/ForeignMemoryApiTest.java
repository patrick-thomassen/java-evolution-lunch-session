package nl.finalist.javaevolution.java15;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import jdk.incubator.foreign.MemorySegment;
import jdk.incubator.foreign.ResourceScope;

@ExtendWith(MockitoExtension.class)
public class ForeignMemoryApiTest {
    @Test
    public void testAllocateNative() {
        MemorySegment segment = null;
        try (ResourceScope scope = ResourceScope.newConfinedScope()) {
            segment = MemorySegment.allocateNative(8, 1, scope);
            System.out.println(segment);
        }
        // Segment is closed now
    }
}
