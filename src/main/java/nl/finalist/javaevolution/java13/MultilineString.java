package nl.finalist.javaevolution.java13;

public class MultilineString {

    public static String getHeader() {
        return """
        Java
        Finally
        Supports
        Multiline
        Strings
        """;
    }
}