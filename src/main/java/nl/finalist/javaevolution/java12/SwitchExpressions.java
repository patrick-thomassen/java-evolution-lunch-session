package nl.finalist.javaevolution.java12;

public class SwitchExpressions {
    public enum Smaak {
        CHOCOLADE,
        VANILLE,
        AARDBEI,
        FRAMBOOS,
        OREO,
        KOKOS
    }

    public static int getPrijs(Smaak smaak) {
        return switch (smaak) {
            case CHOCOLADE, KOKOS -> 3;
            case VANILLE, OREO    -> 4;
            case AARDBEI          -> 5;
            case FRAMBOOS         -> 6;
            default -> throw new IllegalStateException("Invalid value: " + smaak);
        };
    }

    public static int getPrijsWithYield(Smaak smaak) {
        return switch (smaak) {
            case CHOCOLADE:
            case KOKOS:
                yield 3;
            case VANILLE:
            case OREO:
                yield 4;
            case AARDBEI:
                yield 5;
            case FRAMBOOS:
                yield 6;
            default:
                throw new IllegalStateException("Invalid value: " + smaak);
        };
    }
}