package nl.finalist.javaevolution.java14;

public class PatternMatchingInstanceOf {
    public static void giveCommand(Animal animal) {
        // You can directly assign the instanceof now
        if (animal instanceof Dog dog) {
            dog.sit();
        } else if (animal instanceof Cat cat) {
            cat.pickup();
        }
    }
}