package nl.finalist.javaevolution.java14;

public class Dog extends Animal {
    public void sit() {
        System.out.println("Dog is sitting!");
    }
}