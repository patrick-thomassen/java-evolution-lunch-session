package nl.finalist.javaevolution.java14;

public class Cat extends Animal {
    public void pickup() {
        System.out.println("Cat is picked up!");
    }
}
