package nl.finalist.javaevolution.java14;

public record Car (String brand, String model) {
    // constructors, getters, equals, hashCode, toString will all be handled
}