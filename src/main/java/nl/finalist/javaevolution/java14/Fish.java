package nl.finalist.javaevolution.java14;

public class Fish extends Animal {
    private boolean wet;

    public Fish(boolean wet) {
        this.wet = wet;
    }

    public boolean isWet() {
        return wet;
    }

    public void swim() {
        System.out.println("Fish is swimming!");
    }

    public void die() {
        System.out.println("Fish is dying!");
    }
}