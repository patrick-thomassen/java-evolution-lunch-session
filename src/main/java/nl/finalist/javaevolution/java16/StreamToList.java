package nl.finalist.javaevolution.java16;

import java.util.List;

public class StreamToList {
    public static List<String> getList() {
        List<String> animals = List.of("Dog", "Cat", "Fish", "Horse", "Shark");
        return animals.stream()
                .filter(a -> a.contains("o"))
                .toList(); // Just a little shorter
    }
}