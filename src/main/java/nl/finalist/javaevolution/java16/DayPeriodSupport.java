package nl.finalist.javaevolution.java16;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class DayPeriodSupport {
    public static String getPeriodOfDay() {
        LocalTime date = LocalTime.parse("15:25:08.690791");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("h B");
        return date.format(formatter);
    }
}