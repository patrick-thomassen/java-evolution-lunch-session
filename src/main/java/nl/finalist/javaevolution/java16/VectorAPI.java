package nl.finalist.javaevolution.java16;

import jdk.incubator.vector.*;

public class VectorAPI {

    public static FloatVector getResult() {
        float[] a = new float[] { 1f, 2f,  3f, 4f  };
        float[] b = new float[] { 5f, 8f, 10f, 12f };

        FloatVector first = FloatVector.fromArray(FloatVector.SPECIES_128, a, 0);
        FloatVector second = FloatVector.fromArray(FloatVector.SPECIES_128, b, 0);

        return first
            .add(second)
            .pow(2)
            .neg();
    }
}