package nl.finalist.javaevolution.java11;

import java.util.List;
import java.util.function.Predicate;

public class NotPredicateMethod {
    public static List<String> filterBlank(List<String> values) {
        return values.stream()
            .filter(Predicate.not(String::isBlank))
            .toList();
    }
}
