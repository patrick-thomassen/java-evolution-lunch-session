package nl.finalist.javaevolution.java11;

import java.util.List;

import org.eclipse.jdt.annotation.NonNull;

public class LocalVariableLambda {
    public static List<String> upperCaseAll(List<String> values) {
        // You can create lambdas with local var type inference now, allowing the use of annotations on it
        return values.stream().map((@NonNull var a) -> a.toUpperCase()).toList();
    }
}