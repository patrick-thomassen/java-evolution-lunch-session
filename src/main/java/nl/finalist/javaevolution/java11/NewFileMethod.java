package nl.finalist.javaevolution.java11;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class NewFileMethod {
    public static String writeAndReadStringToFile(String value) throws IOException {
        String tempFolder = System.getProperty("java.io.tmpdir");

        // Write to a temp file with Files.createTempFile() and Files.writeString()
        Path tempPath = Files.createTempFile("test", ".txt");
        tempPath.toFile().deleteOnExit();
        Files.writeString(tempPath, value);

        // Read from the temp file with Path.of() and Files.readString()
        tempPath = Path.of(tempFolder, tempPath.getFileName().toString());
        return Files.readString(tempPath);
    }
}