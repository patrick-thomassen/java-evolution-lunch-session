package nl.finalist.javaevolution.java11;

import java.util.List;

public class CollectionToArray {
    public static String[] getNames() {
        List<String> list = List.of("Jan", "Henk", "Sander");
        return list.toArray(String[]::new);
    }
}