package nl.finalist.javaevolution.java17;

import nl.finalist.javaevolution.java14.Animal;
import nl.finalist.javaevolution.java14.Cat;
import nl.finalist.javaevolution.java14.Dog;
import nl.finalist.javaevolution.java14.Fish;

@SuppressWarnings("preview")
public class PatternMatchingSwitch {
    public static Object getValue(Object o) {
        return switch (o) {
            case Integer i -> String.format("int %d", i);
            case Long l    -> String.format("long %d", l);
            case Double d  -> String.format("double %f", d);
            case String s  -> String.format("String %s", s);
            default        -> o.toString();
        };
    }

    // No more instanceof needed here
    public static void giveCommandWithSwitch(Animal animal) {
        switch (animal) {
            case null    -> System.out.println("No animal to command"); // Also: null values
            case Cat cat -> cat.pickup();
            case Dog dog -> dog.sit();
            case Fish fish && fish.isWet() -> fish.swim(); // Combine
            case Fish fish && !fish.isWet() -> fish.die(); // Combine
            default      -> throw new IllegalStateException("Unknown type of animal");
        };
    }
}