module nl.finalist.javaevolution {
    requires jdk.incubator.vector;
    requires java.net.http;
    requires org.eclipse.jdt.annotation;
    requires jdk.incubator.foreign;
    requires jdk.jfr;
}